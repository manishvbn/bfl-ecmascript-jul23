// let result = Math.pow(2, 4);
// console.log(result);

// let result1 = 2 ** 4;
// console.log(result1);

// let result3 = (-3) ** 7;
// console.log(result3);

// --------------------------------------------

let arr = ["ReactJS", "Angular", "ExtJS"];

// if (arr.indexOf('ReactJS')) {
//     console.log("React is avaiable");
// } else {
//     console.error("React is not avaiable");
// }

// if (arr.indexOf('VueJS')) {
//     console.log("Vue JS is avaiable");
// } else {
//     console.error("Vue JS is not avaiable");
// }

if (arr.includes('ReactJS')) {
    console.log("React is avaiable");
} else {
    console.error("React is not avaiable");
}

if (arr.includes('VueJS')) {
    console.log("Vue JS is avaiable");
} else {
    console.error("Vue JS is not avaiable");
}