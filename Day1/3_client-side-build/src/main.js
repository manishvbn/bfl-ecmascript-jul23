// const obj = {
//     arr: [1, 2, 3, 4, 5],
//     printArr() {
//         console.log(...this.arr);           // Array Spread
//     },
//     find(dataToFind) {
//         return this.arr.find(n => n === dataToFind);
//     }
// };

// obj.printArr();
// console.log(obj.find(10));

// Import an entire module, for side effects only, without importing anything from the file.
// This will run the module's global code, but doesn't import any values.
// import './1_datatypes/1_declarations';
// console.log("Main, a is:", a);

// import './1_datatypes/2_es6-declarations';
// import './1_datatypes/3_const';
// import './1_datatypes/4_operators';
// import './1_datatypes/5_symbol';

// import './2_functions/1_fn_creation';
// import './2_functions/2_fn_parameters';
// import './2_functions/3_rest-spread';
import './2_functions/4_pure-impure';